using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RimuTec.AspNetCore.SpaServices.WebpackDevelopmentServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBookApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddHttpClient("fakeapi", client =>
            {
                client.BaseAddress = new Uri("https://fakerestapi.azurewebsites.net/");
            });
            services.AddSpaStaticFiles(configuration =>
            {
                // This is where files will be served from in non-Development environments
                configuration.RootPath = "BookFrontEnd/dist"; // In Development environments, the content of this folder will be deleted
            });
            // add cors in the services 
            services.AddCors(options =>
            {
                options.AddPolicy("CorsApi",
                    builder => builder.WithOrigins("*")
                .AllowAnyHeader()
                .AllowAnyMethod());
            });
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            // add cors in the builder 
            app.UseCors("CorsApi");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "BookFrontEnd/src";

                if (env.IsDevelopment()) // "Development", not "Debug" !!
                {
                    spa.UseWebpackDevelopmentServer(npmScriptName: "start");
                }
            });
        }
    }
}
