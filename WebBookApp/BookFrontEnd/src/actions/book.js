﻿import {
    SAVE_SUCCESS,
    SAVE_FAIL,
    LOGIN_SUCCESS,
    AUTH_SUCCESS,
    AUTH_FAIL,
    LOGIN_FAIL,
    LOGOUT,
    SET_MESSAGE,

} from "./types";
import bookService from "../services/bookService";
export const add = () => (dispatch) => {
    return bookService.add(
        title,
        description,
        pageCount,
        excerpt,
        publishDate
    ).then(
        (response) => {
            dispatch({
                type: SAVE_SUCCESS,
            });
            dispatch({
                type: SET_MESSAGE,
                payload: response,
            });
            return Promise.resolve();
        },
        (error) => {
            const message = error.response
            dispatch({
                type: SAVE_FAIL,
            });
            dispatch({
                type: SET_MESSAGE,
                payload: message,
            });
            return Promise.reject();
        }
    );
};
//export const login = (email: string, password: string, navigate: any) => (dispatch: Dispatch) => {
//    return AuthService.login(email, password, navigate).then(
//        (data) => {
//            dispatch({
//                type: LOGIN_SUCCESS,
//                payload: { user: data },
//            });
//            return Promise.resolve();
//        },
//        (error) => {
//            const message = error.response.data.result || error.response;
//            dispatch({
//                type: LOGIN_FAIL,
//            });
//            dispatch({
//                type: SET_MESSAGE,
//                payload: message,
//            });
//            return Promise.reject();
//        }
//    );
//};
//export const auth = (token: string) => (dispatch: Dispatch) => {
//    return AuthService.auth(token).then(
//        (data) => {
//            dispatch({
//                type: AUTH_SUCCESS,
//                payload: data,
//            });
//            return Promise.resolve();
//        },
//        (error) => {
//            const message = error.response.data.result || error.response;
//            dispatch({
//                type: AUTH_FAIL,
//            });
//            dispatch({
//                type: SET_MESSAGE,
//                payload: message,
//            });
//            localStorage.removeItem("user");
//            localStorage.removeItem("token");
//            return Promise.reject();
//        }
//    );
//};

//};