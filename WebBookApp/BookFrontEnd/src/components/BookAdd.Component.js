﻿import React, { Component, useState } from "react";
import { connect, useDispatch } from "react-redux";
import { add } from "../actions/book";
import { Formik, Form, ErrorMessage, Field } from "formik";
import * as Yup from "yup";
function BookAdd() {
    const [title, settitle] = useState("");
    const [description, setdescription] = useState("");
    const [pageCount, setpageCount] = useState(0);
    const [excerpt, setexcerpt] = useState("");
    const [publishDate, setpublishDate] = useState("");
    const dispatch = useDispatch()
    function validationSchema() {
        return Yup.object().shape({
            title: Yup.string().required("This field is required!"),
            description: Yup.string().required("This field is required!"),
            pageCount: Yup.string().required("This field is required!"),
            excerpt: Yup.string().required("This field is required!"),
        });
        }
        function handleRegister() {
            dispatch(add(title, description, pageCount, excerpt, publishDate))
        }
        const initialValues = {
            title: "",
            description: "",
            pageCount: "",
            excerpt: "",
            publishDate:""
    };
    console.log('hey');
        return (
                <div className="col-md-12">
                <div className="card card-container">
                    <button id="get">Send Get</button>
                    <button id="post">Send Post</button>
                    <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleRegister}>
                            <Form>
                                <div className="row">
                                    <div className="col l7 s12 m12 vertical-height-center auth-page__form-box">
                                        <div className="container">
                                      
                                            <div className="input-field">
                                            <Field name="title" type="text" id="title" className="validate" />
                                            <label htmlFor="title">Title</label>
                                            <ErrorMessage name="title" component="div" className="alert alert-danger" />
                                            </div>
                                            <div className="input-field">
                                            <Field name="description" type="text" id="description" className="validate"  />
                                            <label htmlFor="description">description</label>
                                            <ErrorMessage name="description" component="div" className="alert alert-danger" />
                                            </div>
                                            <div className="input-field">
                                            <Field name="pageCount" type="pageCount" id="pageCount" className="validate"  />
                                            <label htmlFor="pageCount">pageCount</label>
                                            <ErrorMessage name="pageCount" component="div" className="alert alert-danger" />
                                            </div>
                                            <div className="input-field">
                                            <Field name="excerpt" type="text" id="excerpt" className="validate"  />
                                            <label htmlFor="excerpt">Excerpt</label>
                                            <ErrorMessage name="excerpt" component="div" className="alert alert-danger" />
                                        </div>
                                        <div className="input-field">
                                            <Field name="publishDate" type="text" id="publishDate" className="validate"  />
                                            <label htmlFor="publishDate">PublishDate</label>
                                            <ErrorMessage name="publishDate" component="div" className="alert alert-danger" />
                                        </div>
                                            <button type="submit" className="waves-effect waves-light btn-large orange mt-2">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </Form>
                        </Formik>
                    </div>
                </div>
            );
        }
function mapStateToProps(state) {
    const { message } = state.message;
    return {
        message,
    };
}
export default connect(mapStateToProps)(BookAdd);
