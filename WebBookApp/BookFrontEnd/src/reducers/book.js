﻿import {
    SAVE_SUCCESS,
    SAVE_FAIL,
} from "../actions/types";
const initialState = {}
export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case SAVE_SUCCESS:
            return {
                ...state,
                statuscode: 200,
            };
        case SAVE_FAIL:
            return {
                ...state,
                statuscode: 400,
            };
        default:
            return state;
    }
}
