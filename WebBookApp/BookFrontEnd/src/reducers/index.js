﻿import { combineReducers } from "redux";
import book from "./book";
import message from "./message";

export default combineReducers({
    book,
    message,
});
