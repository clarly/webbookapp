﻿const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const isDevelopment = process.env.NODE_ENV !== 'production';
console.log("Using NODE_ENV = " + process.env.NODE_ENV);
var path = require('path');
module.exports = {
    mode: isDevelopment ? 'development' : 'production',
    entry: path.join(__dirname, './src/index.js'),
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    devServer: {
        proxy: {
            "/serve": {
                target: "http://localhost:8000",
                pathRewrite: { "^/serve": "/sockjs-node" },
            },
        }
    },

  module: {
        rules: [
            {
                test: /\.(t|j)sx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            minimize: !isDevelopment
                        }
                    }
                ]
            },
            // https://stackoverflow.com/a/39999421/411428
            {
                // Documentation for url-loader at https://webpack.js.org/loaders/url-loader/
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: false
                        }
                    }
                ]
            },
           
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
  
  },
      output: {
        filename: isDevelopment ? '[name].js' : '[name].[hash].js'
    },
  plugins: [
  
      new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['./dist/*']
        }),
      
      new HtmlWebPackPlugin({
            template: './src/index.html', // template to use
            filename: 'index.html' // name to use for created file
        })
    ]
};