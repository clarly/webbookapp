﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebBookApp.Models;

namespace WebBookApp.Controllers
{
    [Route("api/")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public BookController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        [HttpGet("Books")]
        public async Task<List<Obj>> GetBooks()
        {
            var client = _httpClientFactory.CreateClient("fakeapi");
            var result = await client.GetAsync("api/v1/Books");
            var content = await result.Content.ReadAsStringAsync();
            var valu = JsonConvert.DeserializeObject<List<Obj>>(content);
            return valu;
        }
        [HttpGet("Books/{id}")]
        public async Task<IActionResult> GetBook(int id)
        {
            var client = _httpClientFactory.CreateClient("fakeapi");
            var result = await client.GetAsync("api/v1/Books/1");
            var content = await result.Content.ReadAsStringAsync();
            var valu = JsonConvert.DeserializeObject<Obj>(content);
            return Ok(valu);
        }
        [HttpPost("Books")]
        public async Task<IActionResult> Post(Obj person)
        {
            var client = _httpClientFactory.CreateClient("fakeapi");
            var json = JsonConvert.SerializeObject(person);
            var data = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            var response = await client.PostAsync("api/v1/Books/", data);
            var responseString = await response.Content.ReadAsStringAsync();
            var valu = JsonConvert.DeserializeObject<Obj>(responseString);
            return Ok(valu);

        }
        [HttpPut("Books/{id}")]
        public async Task<IActionResult> Put(int id, Obj person)
        {
            var client = _httpClientFactory.CreateClient("fakeapi");
            var json = JsonConvert.SerializeObject(person);
            var data = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            var response = await client.PutAsync("api/v1/Books/"+id, data);
            var responseString = await response.Content.ReadAsStringAsync();
            var valu = JsonConvert.DeserializeObject<Obj>(responseString);
            return Ok(valu);
        }

        [HttpDelete("Books/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var client = _httpClientFactory.CreateClient("fakeapi");
            var response = await client.DeleteAsync("api/v1/Books/"+id);
            var valu= response.EnsureSuccessStatusCode();
            return Ok(valu.StatusCode);
        }
    }
}
