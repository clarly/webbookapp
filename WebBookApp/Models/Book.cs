﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace WebBookApp.Models
{
    public class Obj
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("title")]
        public string title { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("pageCount")]
        public int pageCount { get; set; }
        [JsonProperty("excerpt")]
        public string excerpt { get; set; }
        [JsonProperty("publishDate")]
        public DateTime publishDate { get; set; }
    }
    public class Book
    {
        public List<Obj> data { get; set; }
    }

}
